<?php

namespace App\Http\Controllers;
use App\Soulmate;

use Illuminate\Http\Request;

class SoulmatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $data = Soulmate::orderBy('id','desc')->paginate(8);
        return  view('index',['data' => $data]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    public function search(Request $request)
    {

        $searchkey = \Request::get('search');
        $data = Soulmate::where('first_name','like','%' .$searchkey. '%')
            ->orderBy('id')->paginate(8);
        return view('index',['data' => $data]);


    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'first_name' => 'required',
            'last_name'  =>  'required',
            'image'     =>  'required|image|max:2048'

        ]);


        $image = $request->file('image');

        $new_name = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('images'), $new_name);
        $form_data = array(
            'first_name'  => $request->first_name,
            'last_name'  => $request->last_name,
            'image'   => $new_name

        );
        Soulmate::create($form_data);

        return redirect('soulmate')->with('success', 'Data Added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Soulmate::findOrFail($id);
        return view('view', compact('data'));





    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Soulmate::findOrfail($id);
        return view('edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image_name = $request->hidden_image;
        $image = $request->file('image');
        if ($image != '')
        {
           $request->validate([
              'first_name' => 'required',
              'last_name' => 'required',
               'image' => 'image|max:2048'
           ]);
           $image_name = rand() . '.' . $image->getClientOriginalExtension();
           $image->move(public_path('images'), $image_name);
        }
        else
            {
            $request->validate([
                'first_name' => 'required',
                'last_name' => 'required',
                'image' => 'image|max:2048'
            ]);
        }
        $form_data = array(
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'image' => $image_name
        );
        Soulmate::whereId($id)->update($form_data);
        return redirect('soulmate')->with('success', 'Data is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Soulmate::findOrFail($id);
        $data->delete();
        return redirect('soulmate')->with('success', 'Data is successfully deleted');
    }
}
