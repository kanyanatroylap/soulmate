<?php $__env->startSection('main'); ?>




<div class="row">
<div class="col-md-6">

</div>
 <div class="col-md-4">
     <form action="<?php echo e(url("/search")); ?>"method="get">
         <div class="input-group-mb3">
             <input type="search" name="search" class="form-control" placeholder="search...">
             <span  class="input-group-append">
                 <button  style="margin-top:  -3.4rem; margin-left: 35rem;"   type="submit" class="btn btn-success">Search</button>
             </span>
         </div>
        </form>
     </div>
         

    <div class="col-md-2 text-right">

        <a href="<?php echo e(route('soulmate.create')); ?>" class="btn btn-primary ">Add</a>

    </div>




</div>


<?php if($message =  Session::get('success')): ?>
    <div class="alert alert-info">
        <p><?php echo e($message); ?></p>
    </div>
    <?php endif; ?>

    <table class="table table-bordered">
        <tr>
            <th width="10%">Profile</th>
            <th width="35%">Name</th>
            <th width="35%">Detail</th>
            <th width="30%">Action</th>
        </tr>
        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><img src="<?php echo e(URL::to('/')); ?>/images/<?php echo e($row->image); ?>" class="img-thumbnail" width="75" /></td>
                <td><?php echo e($row->first_name); ?></td>
                <td><?php echo e($row->last_name); ?></td>
                <td>

                    <form action="<?php echo e(route('soulmate.destroy', $row->id)); ?>"
                          method="post">
                  <a href="<?php echo e(route('soulmate.show', $row->id)); ?>" class="btn btn-info" >Select</a>
                    <a href="<?php echo e(route('soulmate.edit', $row->id)); ?>" class="btn btn-warning">Edit</a>


                        <?php echo csrf_field(); ?>
                        <?php echo method_field('DELETE'); ?>
                        <button type="submit" class="btn btn-danger ">Delete</button>
                    </form>



                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </table>





    <?php echo $data->links(); ?>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('parent', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/soulmate/resources/views/index.blade.php ENDPATH**/ ?>