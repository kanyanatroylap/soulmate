<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Soulmate Pets</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
    <img width="100%" src="<?php echo asset('assets/img/banner3.jpg'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo asset('assets/css/style.css'); ?>">
</head>
<body>
<div class="container">
    <br />
    <h1 class="text-danger" align="center">SOULMATE PETS</h1>
    <br />
    @yield('main')

</div>
<div class="footer">
    <img width="100%" src="<?php echo asset('assets/img/footer3-01.jpg'); ?>">
</div>
</body>
</html>


