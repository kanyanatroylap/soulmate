@extends('parent')

@section('main')


{{--    *********--}}

<div class="row">
<div class="col-md-6">
{{--<h1>SOULMATE PETS</h1>--}}
</div>
 <div class="col-md-4">
     <form action="{{url("/search")}}"method="get">
         <div class="input-group-mb3">
             <input type="search" name="search" class="form-control" placeholder="search...">
             <span  class="input-group-append">
                 <button  style="margin-top:  -3.4rem; margin-left: 35rem;"   type="submit" class="btn btn-success">Search</button>
             </span>
         </div>
        </form>
     </div>
         {{-----------}}

    <div class="col-md-2 text-right">

        <a href="{{ route('soulmate.create') }}" class="btn btn-primary ">Add</a>

    </div>




</div>
{{--row--}}

@if($message =  Session::get('success'))
    <div class="alert alert-info">
        <p>{{ $message }}</p>
    </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th width="10%">Profile</th>
            <th width="35%">Name</th>
            <th width="35%">Detail</th>
            <th width="30%">Action</th>
        </tr>
        @foreach($data as $row)
            <tr>
                <td><img src="{{ URL::to('/') }}/images/{{ $row->image }}" class="img-thumbnail" width="75" /></td>
                <td>{{ $row->first_name }}</td>
                <td>{{ $row->last_name }}</td>
                <td>

                    <form action="{{ route('soulmate.destroy', $row->id) }}"
                          method="post">
                  <a href="{{ route('soulmate.show', $row->id) }}" class="btn btn-info" >Select</a>
                    <a href="{{ route('soulmate.edit', $row->id) }}" class="btn btn-warning">Edit</a>


                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger ">Delete</button>
                    </form>



                </td>
            </tr>
        @endforeach
    </table>





    {!! $data->links() !!}
@endsection

